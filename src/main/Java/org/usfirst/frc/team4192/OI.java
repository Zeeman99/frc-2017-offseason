package org.usfirst.frc.team4192;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import org.usfirst.frc.team4192.commands.ClimbUp;
import org.usfirst.frc.team4192.commands.SensitivityControl;

public class OI {
    private Joystick controller = new Joystick(0);

    public OI(){
        JoystickButton x = new JoystickButton(controller, 3);
        JoystickButton y = new JoystickButton(controller, 4);
        JoystickButton a = new JoystickButton(controller, 1);
        JoystickButton b = new JoystickButton(controller, 2);
        JoystickButton rb = new JoystickButton(controller, 6);
        JoystickButton lb = new JoystickButton(controller, 5);
        JoystickButton start = new JoystickButton(controller, 8);
        JoystickButton back = new JoystickButton(controller,7);

        rb.whileHeld(new ClimbUp());
        lb.toggleWhenPressed(new SensitivityControl());

    }

    public Joystick getController(){
        return controller;
    }

    private double createDeadzone(double axisValue) {
        return Math.abs(axisValue) < 0.05 ? 0 : axisValue;
    }

    public double getXaxis() {
        return createDeadzone(controller.getRawAxis(4));
    }

    public double getYaxis() {
        return createDeadzone(controller.getY());
    }



}
