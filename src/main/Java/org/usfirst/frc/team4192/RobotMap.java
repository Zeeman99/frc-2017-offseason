package org.usfirst.frc.team4192;


public class RobotMap {
    //Motor Controllers
    public static int frontRight = 3;
    public static int backRight  = 4;
    public static int frontLeft  = 8;
    public static int backLeft   = 7;
    public static int climber    = 2;

    //Camera Settings
    public static int IMG_WIDTH  = 320;
    public static int IMG_HEIGHT = 240;
    public static int exposureValue = 40;
    
  //Drive Sensitivty
    public static double driveSensitivity = .85;
}
  /*
  public static int intakeWheelID   = 1;
  public static int liftID          = 2;
  public static int rightMasterID   = 3;
  public static int rightSlaveID    = 4;
  public static int intakeArmID     = 5;
  public static int leftSlaveID     = 7;
  public static int leftMasterID    = 8;*/
