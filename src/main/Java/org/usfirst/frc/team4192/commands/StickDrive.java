package org.usfirst.frc.team4192.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team4192.Robot;
import org.usfirst.frc.team4192.RobotMap;

/**
 * Default command to drive the robot with joysticks. It may be overridden during autonomous mode to run drive sequence. This command grabs
 * the left and right joysticks on an XBox controller and sets them to arcade drive.
 */
public class StickDrive extends Command {

    public StickDrive() {
        requires(Robot.driveTrain);
    }

    protected void initialize() {
    }

    protected void execute() {
        Robot.driveTrain.arcadeDrive(-Robot.oi.getYaxis() * RobotMap.driveSensitivity, -Robot.oi.getXaxis() * RobotMap.driveSensitivity);
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
        Robot.driveTrain.arcadeDrive(0, 0);
    }

    protected void interrupted() {

    }
}