package org.usfirst.frc.team4192.commands.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team4192.commands.*;

public class Baseline extends CommandGroup {

    /*
    Auto Mode For Passing the Baseline
    1. Drive Forward 90 Inches
    */

    public Baseline() {
        addSequential(new DriveDistance(60));
    }
}
