package org.usfirst.frc.team4192.commands.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team4192.commands.*;

public class Middle extends CommandGroup {

    /*
    Auto Mode For Scoring Middle Gear
    1. Drive Forward 75 Inches
    */

    public Middle() {
        addSequential(new DriveDistance(75));
    }
}
