package org.usfirst.frc.team4192.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team4192.Robot;

public class ClimbUp extends Command {

    public ClimbUp(){
        requires(Robot.climb);
    }

    @Override
    protected void execute() {
        Robot.climb.on();
    }

    @Override
    protected void end() {
        Robot.climb.off();
    }

    @Override
    protected void interrupted() {
        end();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
