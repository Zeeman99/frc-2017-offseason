package org.usfirst.frc.team4192.commands;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team4192.Robot;


public class DriveDistance extends Command {
    private PIDController pidDistance;
    private PIDController pidTurn;
    
    private double forward;
    private double turn;

    public DriveDistance(double inches) {
        requires(Robot.driveTrain);

        forward = 0;
        turn    = 0;
        pidDistance = new PIDController(0.00006, 0.000001, 0, new pidSource(), new pidOutput());
        pidTurn = new PIDController(0.23, 0.006, 0, Robot.gyro.getGyro(), new pidOutputTurn());

        pidDistance.setAbsoluteTolerance(.0005);
        pidTurn.setAbsoluteTolerance(.25);

        pidDistance.setOutputRange(-1,1);
        pidTurn.setOutputRange(-.5,.5);

        SmartDashboard.putNumber("Target Drive Ticks", inchesToTicks(inches));
        SmartDashboard.putNumber("Target Gyro Angle", 0);

        pidDistance.setSetpoint(inchesToTicks(inches));
        pidTurn.setSetpoint(0);
    }

    @Override
    protected void initialize() {
        Robot.driveTrain.zeroEncoders();
        Robot.gyro.reset();

        pidDistance.enable();
        pidTurn.enable();
      SmartDashboard.putBoolean("Drive Forward Running",true);
    }

    @Override
    protected void execute() {
        Robot.driveTrain.arcadeDrive(forward,turn);
    }

    @Override
    protected void end() {
        pidTurn.disable();
        pidTurn.reset();
        pidDistance.disable();
        pidDistance.reset();
        Robot.driveTrain.arcadeDrive(0,0);
        SmartDashboard.putBoolean("Drive Forward Running",false);
    }

    @Override
    protected void interrupted() {
        end();
    }

    @Override
    protected boolean isFinished() {
        return pidDistance.onTarget();
    }

    private double inchesToTicks(double inches){
        return (inches/(6 * Math.PI)) * 4096;
    }

    private class pidOutput implements PIDOutput {
        @Override
        public void pidWrite(double output) {
            forward=output;
        }
    }

    private class pidOutputTurn implements PIDOutput {
        @Override
        public void pidWrite(double output) {
            turn=-output;
        }
    }

    private class pidSource implements PIDSource{
        @Override
        public double pidGet() {
            return Robot.driveTrain.getRightEncoder();
        }

        @Override
        public void setPIDSourceType(PIDSourceType pidSource){}

        @Override
        public PIDSourceType getPIDSourceType() {
            return PIDSourceType.kDisplacement;
        }
    }
}
