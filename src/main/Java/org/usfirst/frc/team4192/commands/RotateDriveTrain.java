package org.usfirst.frc.team4192.commands;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team4192.Robot;

/**
 * This command rotates the drive train to a given degree.
 */
public class RotateDriveTrain extends Command{
    private double theta=0;
    private PIDController pid;

    /**
     *
     * @param theta degrees to rotate drive train.
     */
    public RotateDriveTrain(double theta){
        requires(Robot.driveTrain);
        requires(Robot.gyro);

        this.theta=theta;
        pid = new PIDController(0.55, 0.002, 0.4, Robot.gyro.getGyro(), new pidOutput());
        pid.setAbsoluteTolerance(.5);
        pid.setInputRange(-360,360);
        pid.setOutputRange(-0.65,0.65);
        pid.setSetpoint(theta);
        pid.startLiveWindowMode();
    }

    @Override
    protected void initialize() {
        Robot.driveTrain.zeroEncoders();
        Robot.gyro.reset();
        pid.reset();
        pid.enable();
        SmartDashboard.putNumber("Target Gyro Angle",theta);
    }

    @Override
    protected void execute() {
        SmartDashboard.putNumber("Gyro Angle", Robot.gyro.getAngle());
    }

    /**
     *
     * @return when the PID is on target
     */
    @Override
    protected boolean isFinished() {
        return pid.onTarget();
    }

    /**
     * Stop driving the robot
     */
    @Override
    protected void end() {
        Robot.driveTrain.zeroEncoders();
        Robot.driveTrain.arcadeDrive(0,0);
        pid.disable();
        pid.reset();
    }

    /**
     * Abort driving the robot
     */
    @Override
    protected void interrupted() {
        Robot.driveTrain.zeroEncoders();
        Robot.driveTrain.arcadeDrive(0,0);
        pid.disable();
        pid.reset();
    }

    /**
     * Drives the robot to the PID output using arcade drive.
     */
    private class pidOutput implements PIDOutput {
        @Override
        public void pidWrite(double output) {
            Robot.driveTrain.arcadeDrive(0,-output);
        }
    }
}