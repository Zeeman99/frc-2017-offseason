package org.usfirst.frc.team4192.commands.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team4192.commands.*;

public class Right extends CommandGroup {

    /*
    Auto Mode For Scoring Right Gear
    1. Drive Forward 90 Inches
    2. Turn -45 Degrees
    3. Drive Forward 40 Inches
    */

    public Right() {
        addSequential(new DriveDistance(90));
        addSequential(new RotateDriveTrain(45));
        addSequential(new DriveDistance(40));
    }
}
