package org.usfirst.frc.team4192.commands;


import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team4192.RobotMap;

public class SensitivityControl extends Command{
  @Override
  protected void initialize() {
    
  }
  
  @Override
  protected void execute(){
    RobotMap.driveSensitivity = .5;
  }
  
  @Override
  protected void end(){
    RobotMap.driveSensitivity = .85;
  }
  
  @Override
  protected void interrupted() {
    end();
  }
  
  @Override
  protected boolean isFinished() {
    return false;
  }
}
