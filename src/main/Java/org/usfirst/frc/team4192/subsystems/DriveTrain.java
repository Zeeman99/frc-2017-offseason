package org.usfirst.frc.team4192.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc.team4192.RobotMap;
import org.usfirst.frc.team4192.commands.StickDrive;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;

import com.ctre.CANTalon;

/**
 * This class provides drive functionality to the robot. It uses the two most common forms of standard drive, arcade and tank. Motor ports
 * can be changed in the RobotMap.
 */
public class DriveTrain extends Subsystem {
    private CANTalon frontRight = new CANTalon(RobotMap.frontRight);
    private CANTalon frontLeft = new CANTalon(RobotMap.frontLeft);
    private CANTalon backRight = new CANTalon(RobotMap.backRight);
    private CANTalon backLeft  = new CANTalon(RobotMap.backLeft);
    private RobotDrive drivetrain;

    public DriveTrain() {
        frontLeft.changeControlMode(CANTalon.TalonControlMode.PercentVbus);
        frontRight.changeControlMode(CANTalon.TalonControlMode.PercentVbus);

        frontRight.enableBrakeMode(true);
        backRight.enableBrakeMode(true);
        frontLeft.enableBrakeMode(true);
        backLeft.enableBrakeMode(true);

        frontLeft.setFeedbackDevice(CANTalon.FeedbackDevice.CtreMagEncoder_Relative);
        frontRight.setFeedbackDevice(CANTalon.FeedbackDevice.CtreMagEncoder_Relative);

        frontLeft.reverseOutput(false);
        frontRight.reverseSensor(true);
        frontRight.reverseOutput(true);

        backLeft.changeControlMode(CANTalon.TalonControlMode.Follower);
        backLeft.set(frontLeft.getDeviceID());
        backRight.changeControlMode(CANTalon.TalonControlMode.Follower);
        backRight.set(frontRight.getDeviceID());

        drivetrain = new RobotDrive(frontLeft, frontRight);
    }

    /**
     * Sets the default state of the robot to be driven by the joystick.
     */
    public void initDefaultCommand() {
        setDefaultCommand(new StickDrive());
    }

    public void zeroEncoders() {
        frontRight.setEncPosition(0);
        frontLeft.setEncPosition(0);
    }

    public double getLeftEncoder(){
        return frontLeft.getEncPosition();
    }

    public double getRightEncoder(){
        return frontRight.getEncPosition();
    }

    /**
     * A method to drive the robot based on forward power and rotation.
     * forward the value from -1 to 1 to move the robot in the y-axis
     * turn the value from -1 to 1 to move the robot in the x-axis
     */
    public void arcadeDrive(double forward, double turn) {
        drivetrain.arcadeDrive(forward, turn);
    }

    /**
     * A method the drive the robot using tank style left/right joysticks.
     * joystickLeft joystick for left control
     * joystickRight joystick for right control
     */
    public void tankDrive(Joystick joystickLeft, Joystick joystickRight) {
        drivetrain.tankDrive(joystickLeft, joystickRight);
    }

    /**
     * A method to drive the robot using tank style left/right values.
     * leftValue a value from -1 to 1
     * rightValue a value from -1 to 1
     */
    public void tankDrive(double leftValue, double rightValue) {
        drivetrain.tankDrive(leftValue, rightValue);
    }
}