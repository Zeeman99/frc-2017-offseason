package org.usfirst.frc.team4192.subsystems;

import com.ctre.CANTalon;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc.team4192.RobotMap;

public class Climb extends Subsystem {
    private CANTalon climb = new CANTalon(RobotMap.climber);

    @Override
    protected void initDefaultCommand(){}

    public Climb(){}

    public void on(){
        climb.set(1);
    }

    public void off(){
        climb.set(0);
    }

}
