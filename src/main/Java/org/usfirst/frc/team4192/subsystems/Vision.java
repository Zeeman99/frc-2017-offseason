package org.usfirst.frc.team4192.subsystems;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.command.Subsystem;


public class Vision extends Subsystem {
    private UsbCamera camera;

    public Vision() {
      camera = CameraServer.getInstance().startAutomaticCapture();
    }

    @Override
    protected void initDefaultCommand() {
    }
  
}