package org.usfirst.frc.team4192.subsystems;


import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Gyro extends Subsystem {
    private AHRS gyro = new AHRS(SPI.Port.kMXP);

    public Gyro(){}

    @Override
    protected void initDefaultCommand(){}

    public void reset(){
        gyro.reset();
    }

    public double getAngle(){
        return gyro.getAngle();
    }

    public AHRS getGyro(){
        return gyro;
    }
}
