package org.usfirst.frc.team4192;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team4192.commands.auto.*;
import org.usfirst.frc.team4192.subsystems.Climb;
import org.usfirst.frc.team4192.subsystems.DriveTrain;
import org.usfirst.frc.team4192.subsystems.Gyro;
import org.usfirst.frc.team4192.subsystems.Vision;


public class Robot extends IterativeRobot {

    public static DriveTrain driveTrain;
    public static Climb climb;
    public static Gyro gyro;
    public static Vision vision;

    public static OI oi;
    
    public static double pidError;
  

    private SendableChooser<String> chooser;
    private CommandGroup autonomousCommandGroup;

    private final String noAuton       = "None";
    private final String leftAuton     = "Left Autonomous";
    private final String rightAuton    = "Right Autonomous";
    private final String middleAuton   = "Middle Autonomous";
    private final String baselineAuton = "Baseline Autonomous";
    @Override
    public void robotInit() {
        driveTrain = new DriveTrain();
        climb = new Climb();
        gyro = new Gyro();
        vision = new Vision();
        oi = new OI();
        
        pidError = 0;

        chooser = new SendableChooser<>();
        chooser.addDefault("None", noAuton);
        chooser.addObject("Left Autonomous", leftAuton);
        chooser.addObject("Middle Autonomous", middleAuton);
        chooser.addObject("Right Autonomous", rightAuton);
        chooser.addObject("Baseline", baselineAuton);
        SmartDashboard.putData("Autonomous Choices", chooser);
        autonomousCommandGroup = null;
  
      Thread dashboardUpdateThread = new Thread(() -> {
        while (!Thread.interrupted()) {
          SmartDashboard.putNumber("Gyro Angle",
              gyro.getAngle());
      
          SmartDashboard.putNumber("Right Encoder Ticks",
              driveTrain.getRightEncoder());
          
          SmartDashboard.putNumber("PID Distance Error",
              pidError);
        }
      });
      dashboardUpdateThread.start();
      
      SmartDashboard.putData("Scheduler",Scheduler.getInstance());
    }

    @Override
    public void disabledInit() {
        super.disabledInit();
    }

    @Override
    public void disabledPeriodic() {
        Scheduler.getInstance().run();
    }

    @Override
    public void autonomousInit() {

        String autoSelected = baselineAuton;
        switch (autoSelected) {
            case leftAuton:
                autonomousCommandGroup = new Left();
                break;
            case middleAuton:
                autonomousCommandGroup = new Middle();
                break;
            case rightAuton:
                autonomousCommandGroup = new Right();
                break;
            case baselineAuton:
                autonomousCommandGroup = new Baseline();
                break;
            default:
                autonomousCommandGroup = new NoAuton();
                break;
        }
        if(autonomousCommandGroup!= null)
          autonomousCommandGroup.start();
    }
  
  @Override
    public void autonomousPeriodic() {
        Scheduler.getInstance().run();
    }

    @Override
    public void teleopInit() {
      if(autonomousCommandGroup!=null) autonomousCommandGroup.cancel();
    }
  
  @Override
    public void testPeriodic() {
      Scheduler.getInstance().run();
    }

    @Override
    public void teleopPeriodic() {
        System.out.println("Right Encoder Rotations: " + Robot.driveTrain.getRightEncoder()/4096);
        Scheduler.getInstance().run();
        
    }
}
